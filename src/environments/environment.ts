// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false
};

export const firebaseConfig = {
    apiKey: 'AIzaSyDoTCerrAhWPv0pkhAmR0sTeyr9FKUfGjw',
    authDomain: 'first-project-ad81e.firebaseapp.com',
    databaseURL: 'https://first-project-ad81e.firebaseio.com',
    projectId: 'first-project-ad81e',
    storageBucket: 'first-project-ad81e.appspot.com',
    messagingSenderId: '414891786605',
    appId: '1:414891786605:web:a0d338982e0afd8e625a64'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
