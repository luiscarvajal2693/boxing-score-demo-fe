import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Match } from '../../models/match';
import { faPencilAlt, faSave, faEye } from '@fortawesome/free-solid-svg-icons';
import { Rival } from '../../models/rival';

@Component({
    selector: 'app-battle-home-step',
    templateUrl: './battle-home-step.component.html',
    styleUrls: ['./battle-home-step.component.scss']
})
export class BattleHomeStepComponent {

    faPencil = faPencilAlt;
    faSave = faSave;
    faEye = faEye;

    @Input() match: Match;
    @Output() rivalToShow: EventEmitter<Rival> = new EventEmitter<Rival>();

    showDetails(rival: Rival) {
        this.rivalToShow.emit(rival);
    }
}
