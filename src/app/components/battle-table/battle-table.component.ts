import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Round } from '../../models/round';
import { Category } from '../../models/category';
import { Rival } from '../../models/rival';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-battle-table',
    templateUrl: './battle-table.component.html',
    styleUrls: ['./battle-table.component.scss']
})
export class BattleTableComponent {

    @ViewChild('openModalShowRival') private openModalShowRival: ElementRef;
    @ViewChild('modalShowRival') private modalShowRival;
    @Input() round: Round;
    @Input() category: Category;

    rivalToShow: Rival;
    closeResult = '';

    constructor(private modalService: NgbModal) {
    }

    static getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    showRivalDetails($event) {
        console.log(this.category);

        this.rivalToShow = $event;
        this.openModalShowRival.nativeElement.click();
    }

    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title', centered: true}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.closeResult = `Dismissed ${BattleTableComponent.getDismissReason(reason)}`;
        });
    }
}
