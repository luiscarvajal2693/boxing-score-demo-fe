import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataForPagination } from 'src/app/models/paginatedData';


@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {
    @Input() dataPages: DataForPagination;
    @Output() clickPage = new EventEmitter<DataForPagination>();
    constructor() { }

    ngOnInit(): void {
    }

    getDataByPage() {
        this.clickPage.emit(this.dataPages);
    }

    firstPage() {
        if (this.dataPages.pageInUse > 1) {
            this.dataPages.pageInUse = 1;
            this.dataPages.mainPage = 1;
            this.dataPages.middlePage = Math.trunc(this.dataPages.allPages / 2);
            this.getDataByPage();
        }
    }

    lastPage() {
        if (this.dataPages.pageInUse < this.dataPages.allPages) {
            this.dataPages.pageInUse = this.dataPages.allPages;
            this.dataPages.middlePage = this.dataPages.allPages - 2;
            this.getDataByPage();
        }
    }

    previousPage() {
        if (this.dataPages.pageInUse > 1) {
            this.dataPages.pageInUse = this.dataPages.pageInUse - 1;
            if (this.dataPages.mainPage > 1) {
                if (this.dataPages.mainPage >= this.dataPages.middlePage - 1 ) {
                    this.dataPages.mainPage = 1;
                } else {
                    this.dataPages.mainPage = this.dataPages.mainPage - 1;
                }
            }
            if ( (this.dataPages.middlePage - 1) !== 2 && (this.dataPages.middlePage - 1) < (this.dataPages.allPages - 3)) {
                this.dataPages.middlePage = this.dataPages.middlePage - 1;
            }
            if ( (this.dataPages.middlePage - 1) === this.dataPages.pageInUse &&  this.dataPages.middlePage > 3) {
                this.dataPages.middlePage = this.dataPages.middlePage - 1;
            }
            this.getDataByPage();
        }
    }

    nextPage() {
        if (this.dataPages.pageInUse < this.dataPages.allPages) {
            this.dataPages.pageInUse = this.dataPages.pageInUse + 1;
            this.dataPages.mainPage = this.dataPages.mainPage + 1;
            if ( (this.dataPages.middlePage + 1) !== (this.dataPages.allPages - 1) ) {
                this.dataPages.middlePage = this.dataPages.middlePage + 1;
            }
            this.getDataByPage();
        }
    }

    getPage(page: number) {
        if (this.dataPages.pageInUse !== page) {
            this.dataPages.pageInUse = page;
            this.getDataByPage();
        }
    }


}
