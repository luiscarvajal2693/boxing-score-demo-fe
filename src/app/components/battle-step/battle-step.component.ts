import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Match} from '../../models/match';
import {faPencilAlt, faSave} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-battle-step',
    templateUrl: './battle-step.component.html',
    styleUrls: ['./battle-step.component.scss']
})
export class BattleStepComponent {

    faPencil = faPencilAlt;
    faSave = faSave;

    @Input() matches: Match[];
    @Output() edit: EventEmitter<Match> = new EventEmitter<Match>();
    @Output() save: EventEmitter<Match> = new EventEmitter<Match>();

}
