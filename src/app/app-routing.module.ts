import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BootstrapTableComponent} from './bootstrap-table/bootstrap-table.component';
import {BootstrapFormComponent} from './bootstrap-form/bootstrap-form.component';
import {JumbotronComponent} from './jumbotron/jumbotron.component';
import {LoginComponent} from './pages/login/login.component';
import {HomeComponent} from './pages/home/home.component';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'home' },
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'jumbotron', component: JumbotronComponent },
    { path: 'bootstrap-form', component: BootstrapFormComponent },
    { path: 'bootstrap-table', component: BootstrapTableComponent },
    {
        path: 'admin', loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule)
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
