import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AppService} from '../../services/app.service';
import {Md5} from 'ts-md5';
import {Router} from '@angular/router';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

    loginForm: FormGroup;
    loading = false;
    submitted = false;

    constructor(
        private formBuilder: FormBuilder,
        private services: AppService,
        private router: Router) {
        if (services.isLogged()) {
            this.router.navigate(['admin']).then();
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }

    get f() {
        return this.loginForm.controls;
    }

    onSubmit() {
        this.submitted = true;

        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;
        const username = this.f.username.value;
        const password = Md5.hashStr(this.f.password.value);
        this.services.login(username, password.toString()).then(
            value => {
                this.services.setLogged(true);
                this.router.navigate(['admin']).then();
                this.loading = false;
            },
            error => {
                this.loading = false;
                alert(error.reason);
            }
        );
    }

}
