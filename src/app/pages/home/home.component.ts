import { Component, OnInit } from '@angular/core';
import { faSearch, faUser, faAngleRight, faSkull } from '@fortawesome/free-solid-svg-icons';
import { Round } from '../../models/round';
import { AppService } from '../../services/app.service';
import { Category } from '../../models/category';
import { ToastrService } from 'ngx-toastr';
import { Tournament } from '../../models/tournament';
import {LanguageService} from '../../services/language.service';
import {MessageService} from '../../services/message.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    faUser = faUser;
    faAngleRight = faAngleRight;

    tournaments: Tournament[] = [];
    categories: Category[] = [];
    selectedSex: string;
    selectedTournament: Tournament;
    selectedCategory: Category;
    loadingRivals = false;
    round: Round;
    currentLanguage: string;
    languages: string[];
    previousIndex: number;

    constructor(
        private services: AppService,
        private toastr: ToastrService,
        private languageService: LanguageService,
        private messageService: MessageService) {
        this.currentLanguage = languageService.getCurrentLanguage();
        this.languages = services.getLanguages();
    }

    ngOnInit() {
        this.services.getCategories().then(
            success => {
                this.categories = success;
            }, () => {
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
        // this.services.getTournaments().then(
        //     success => {
        //         this.tournaments = success;
        //     }, () => {
        //         this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
        //             {positionClass: 'toast-center-center'});
        //     }
        // );
        this.services.getAllRounds().then(
            success => {
                this.tournaments = success;
                console.log(success);
            }, () => {
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    onSelectTournament(tournament, index: number, $event: MouseEvent) {
        console.log('-> $event', $event);
        if (!this.previousIndex || this.previousIndex !== index) {
            this.previousIndex = index;
            this.selectedTournament = tournament.tournament;
            this.selectedSex = undefined;
            this.selectedCategory = undefined;
            this.round = undefined;
            this.onSelectCategory(tournament.category);
        }
    }

    onSelectSex(sex: string) {
        this.selectedSex = sex;
        this.selectedCategory = undefined;
        this.round = undefined;
    }

    onSelectCategory(category: Category) {
        this.selectedCategory = category;
        this.loadingRivals = true;
        this.round = undefined;
        this.services.getBattlesRoundByCategory(category.id, this.selectedTournament.id).then(
            (success: Round) => {
                this.loadingRivals = false;
                this.round = success;
                console.log(success);
            }, () => {
                this.loadingRivals = false;
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    getCategoriesBySex(): Category[] {
        const categories: Category[] = [];
        for (const category of this.categories) {
            if (category.sex === this.selectedSex) {
                categories.push(category);
            }
        }
        return categories;
    }

    changeLanguage(language: string) {
        this.currentLanguage = language;
        this.languageService.setDefaultLanguage(language);
    }

    clearCategoryToTranslate(category: string): string {
        return `categories.${category.replace(' ', '').toLowerCase()}`;
    }
}
