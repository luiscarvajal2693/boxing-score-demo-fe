import {Component, OnInit} from '@angular/core';
import {faSearch, faUser} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {AppService} from '../../services/app.service';
import {LanguageService} from '../../services/language.service';

@Component({
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

    faUser = faUser;
    currentLanguage: string;
    languages: string[];

    constructor(private router: Router,
                private services: AppService,
                private languageService: LanguageService) {
        if (!services.isLogged()) {
            this.router.navigate(['login']).then();
        }
        this.currentLanguage = languageService.getCurrentLanguage();
        this.languages = services.getLanguages();
    }

    ngOnInit() {

    }

    logout() {
        this.services.setLogged(false);
        this.router.navigate(['home']).then();
    }

    changeLanguage(language: string) {
        this.currentLanguage = language;
        this.languageService.setDefaultLanguage(language);
    }
}
