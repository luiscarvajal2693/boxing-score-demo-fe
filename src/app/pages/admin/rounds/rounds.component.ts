import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { faTrash, faUser, faSortNumericUpAlt, faCrown, faSkull, faEye } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { Category } from '../../../models/category';
import { AppService } from '../../../services/app.service';
import { ToastrService } from 'ngx-toastr';
import { Match } from '../../../models/match';
import { Tournament } from '../../../models/tournament';
import { Rival } from '../../../models/rival';
import {MessageService} from '../../../services/message.service';

@Component({
    selector: 'app-rounds',
    templateUrl: './rounds.component.html',
    styleUrls: ['./rounds.component.scss']
})

export class RoundsComponent implements OnInit {

    faTrash = faTrash;
    faUser = faUser;
    faSortNumericUpAlt = faSortNumericUpAlt;
    faCrown = faCrown;
    faSkull = faSkull;
    faEye = faEye;

    tournaments: Tournament[] = [];
    categories: Category[] = [];
    rivals: any[] = undefined;
    selectedTournament: Tournament;
    selectedSex: string;
    selectedCategory: Category;
    selectedBattles: any[] = [
        {
            couple: 1,
            date: '',
            members: [
                {
                    id: 1,
                    info: undefined
                },
                {
                    id: 2,
                    info: undefined
                }
            ]
        },
        {
            couple: 2,
            date: '',
            members: [
                {
                    id: 3,
                    info: undefined
                },
                {
                    id: 4,
                    info: undefined
                }
            ]
        },
        {
            couple: 3,
            date: '',
            members: [
                {
                    id: 5,
                    info: undefined
                },
                {
                    id: 6,
                    info: undefined
                }
            ]
        },
        {
            couple: 4,
            date: '',
            members: [
                {
                    id: 7,
                    info: undefined
                },
                {
                    id: 8,
                    info: undefined
                }
            ]
        },
        {
            couple: 5,
            date: '',
            members: [
                {
                    id: 9,
                    info: undefined
                },
                {
                    id: 10,
                    info: undefined
                }
            ]
        },
        {
            couple: 6,
            date: '',
            members: [
                {
                    id: 11,
                    info: undefined
                },
                {
                    id: 12,
                    info: undefined
                }
            ]
        },
        {
            couple: 7,
            date: '',
            members: [
                {
                    id: 13,
                    info: undefined
                },
                {
                    id: 14,
                    info: undefined
                }
            ]
        },
        {
            couple: 8,
            date: '',
            members: [
                {
                    id: 15,
                    info: undefined
                },
                {
                    id: 16,
                    info: undefined
                }
            ]
        }
    ];
    roundBattles: Match[] = undefined;
    lastSelected: Rival = undefined;
    matchSelected: Match = undefined;
    loadingRivals = false;
    savingRound = false;
    deletingRound = false;
    updatingMatch = false;
    errorValidation = false;
    listCompleted = false;

    @ViewChild('buttonCloseModalMatch') private buttonCloseModalMatch: ElementRef;
    @ViewChild('buttonCloseConfirmRoundModal') private buttonCloseConfirmRoundModal: ElementRef;

    constructor(
        private router: Router,
        private appService: AppService,
        private toastr: ToastrService,
        private messageService: MessageService
    ) {
    }


    ngOnInit() {
        this.appService.getCategories().then(
            success => {
                this.categories = success;
            }, () => {
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
        this.appService.getTournaments().then(
            success => {
                this.tournaments = success;
            }, () => {
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );

    }

    onSelectTournament() {
        this.selectedSex = undefined;
        this.selectedCategory = undefined;
        this.roundBattles = undefined;
        this.clearDefaultObject();
    }

    onSelectSex(sex: string) {
        this.selectedSex = sex;
        this.selectedCategory = undefined;
        this.roundBattles = undefined;
        this.clearDefaultObject();
    }

    onSelectCategory(category: Category) {
        this.selectedCategory = category;
        this.roundBattles = undefined;
        this.clearDefaultObject();
        this.getRound(category);
    }

    getRound(category?) {
        this.loadingRivals = true;
        this.appService.getRoundByCategory(this.selectedCategory.id, this.selectedTournament.id).then(
            (success: any[]) => {
                if (category && success.length === 0) {
                    this.roundBattles = undefined;
                    this.getRivals(category);
                } else {
                    this.loadingRivals = false;
                    this.roundBattles = success;
                }
            }, () => {
                this.loadingRivals = false;
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    getCategoriesBySex(): Category[] {
        const categories: Category[] = [];
        for (const category of this.categories) {
            if (category.sex === this.selectedSex) {
                categories.push(category);
            }
        }
        return categories;
    }

    getRivals(category: Category) {
        this.selectedCategory = category;
        this.loadingRivals = true;
        this.appService.getRivalsByCategory(category.id).then(
            success => {
                this.loadingRivals = false;
                this.rivals = success;
            }, () => {
                this.loadingRivals = false;
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    newRival() {
        if (!this.lastSelected) { return; }
        let slot;
        for (const selectedBattle of this.selectedBattles) {
            let member = selectedBattle.members[0];
            if (member.info === undefined) {
                slot = member;
                break;
            }
            member = selectedBattle.members[1];
            if (member.info === undefined) {
                slot = member;
                break;
            }
        }
        if (slot) {
            slot.info = this.lastSelected;
            this.lastSelected.disable = true;
            if (slot.id === 16) {
                this.listCompleted = true;
            }
        } else {
            this.listCompleted = true;
        }
        this.lastSelected = undefined;
    }

    deleteRival(memberI: number, coupleI: number) {
        const aux = this.selectedBattles[coupleI].members[memberI];
        aux.info.disable = false;
        aux.info = undefined;
        if (this.listCompleted) {
            this.listCompleted = false;
        }
    }

    deleteRound() {
        this.buttonCloseConfirmRoundModal.nativeElement.click();
        this.deletingRound = true;
        this.appService.deleteRoundByCategory(this.selectedCategory.id, this.selectedTournament.id).then(
            (success) => {
                this.deletingRound = false;
                this.toastr.success(success.delete + this.messageService.matchesDeleteSuccessfully, this.messageService.done,
                    {positionClass: 'toast-center-center'});
                this.roundBattles = undefined;
                this.getRivals(this.selectedCategory);
            }, () => {
                this.deletingRound = false;
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    saveRound() {
        const battle = this.selectedBattles.filter( selectedBattle => selectedBattle.date === '');
        if (battle.length >= 1) {
            this.toastr.error(this.messageService.youMustAssignDateOfBattles, this.messageService.error,
                {positionClass: 'toast-center-center'});
        } else {
            const rounds: Match[] = [];
            for (const selectedBattle of this.selectedBattles) {
                delete selectedBattle.members[0].disable;
                delete selectedBattle.members[1].disable;
                const match: Match = {
                    date: selectedBattle.date,
                    rival1_id: selectedBattle.members[0].info.id,
                    rival2_id: selectedBattle.members[1].info.id,
                    category_id: this.selectedCategory.id,
                    tournament_id: this.selectedTournament.id
                };
                rounds.push(match);
            }
            this.savingRound = true;
            this.appService.saveRound(rounds).then(
                success => {
                    this.savingRound = false;
                    this.listCompleted = false;
                    this.toastr.success(this.messageService.roundCreatedSuccessfully, this.messageService.save,
                        {positionClass: 'toast-center-center'});
                    this.onSelectCategory(this.selectedCategory);
                }, () => {
                    this.savingRound = false;
                    this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                        {positionClass: 'toast-center-center'});
                }
            );
        }
    }

    saveDateBattle(matchDate) {
        this.appService.updateMatch(matchDate).then(
            success => {
                this.getRound();
                this.toastr.success(this.messageService.dateSavedSuccessfully, this.messageService.save,
                    {positionClass: 'toast-center-center'});
            }, () => {
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    saveScores() {
        this.errorValidation = false;
        if (this.matchSelected.live) {
            for (const match of this.roundBattles) {
                if (match.live === true && match.id !== this.matchSelected.id) {
                    return this.toastr.error(this.messageService.alreadyLiveBattle, this.messageService.error,
                        {positionClass: 'toast-center-center'});
                }
            }
        }
        if (this.matchSelected.rival1_score || this.matchSelected.rival2_score || this.matchSelected.winner) {
            if (!this.matchSelected.winner) {
                return this.errorValidation = true;
            } else
                if (!this.matchSelected.rival1_score || this.matchSelected.rival1_score.length < 1) {
                return this.errorValidation = true;
            } else
                if (!this.matchSelected.rival2_score || this.matchSelected.rival2_score.length < 1) {
                return this.errorValidation = true;
            }
        }
        this.updatingMatch = true;
        this.errorValidation = false;

        this.appService.updateMatch(this.matchSelected).then(
            success => {
                this.updatingMatch = false;
                this.buttonCloseModalMatch.nativeElement.click();
                this.getRound();
            }, () => {
                this.updatingMatch = false;
                this.toastr.error(this.messageService.errorWasOccurred, this.messageService.error,
                    {positionClass: 'toast-center-center'});
            }
        );
    }

    filterByStep(round: Match[], step: number): Match[] {
        const stepList: Match[] = [];
        round.forEach(match => {
            if (match.step === step) {
                stepList.push(match);
            }
        });
        console.log(stepList);
        return stepList;
    }

    cloneObject(clone) {
        this.matchSelected = Object.assign({}, clone);
    }

    clearDefaultObject() {
        for (const selectedBattle of this.selectedBattles) {
            selectedBattle.date = '';
            selectedBattle.members[0].info = undefined;
            selectedBattle.members[1].info = undefined;
        }
    }

    clearCategoryToTranslate(category: string): string {
        return `categories.${category.replace(' ', '').toLowerCase()}`;
    }
}
