import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin.component';
import { RivalsComponent } from './rivals/rivals.component';
import { TournamentsComponent } from './tournaments/tournaments.component';
import { RoundsComponent } from './rounds/rounds.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [
    {
        path: '', component: AdminComponent, children:
            [
                {
                    path: 'home',
                    component: HomeComponent
                },
                {
                    path: 'rivals',
                    component: RivalsComponent
                },
                {
                    path: 'tournaments',
                    component: TournamentsComponent
                },
                {
                    path: 'rounds',
                    component: RoundsComponent
                },
                {
                    path: '',
                    redirectTo: '/admin/home',
                    pathMatch: 'full'
                }
            ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AdminRoutingModule {
}
