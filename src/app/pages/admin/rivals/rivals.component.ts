import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {faTrash, faVenusMars, faFlag, faPlus, faEdit, faCamera} from '@fortawesome/free-solid-svg-icons';
import {Router} from '@angular/router';
import {AppService} from '../../../services/app.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Rival} from '../../../models/rival';
import {ToastrService} from 'ngx-toastr';
import {FireStorage} from '../../../services/fire.storage';
import {PaginatedData, DataForPagination} from 'src/app/models/paginatedData';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {MessageService} from '../../../services/message.service';

@Component({
    selector: 'app-rivals',
    templateUrl: './rivals.component.html',
    styleUrls: ['./rivals.component.scss']
})
export class RivalsComponent implements OnInit {

    faTrash = faTrash;
    faVenusMars = faVenusMars;
    faFlag = faFlag;
    faPlus = faPlus;
    faEdit = faEdit;
    faCamera = faCamera;

    countries: any[] = [];
    rivals: Rival[] = [];
    rivalForm: FormGroup;

    action = '';
    submitted = false;
    loading = false;
    loadingRivals = false;
    selectedRival: Rival;
    selectedRivalEdit: number;
    fileData: File;
    dataForPagination: DataForPagination = {
        pageInUse: 1,
        allPages: 0,
        pages: [],
        middlePage: 0,
        mainPage: 1,
    };
    @ViewChild('openModalFormRival') private openModalFormRival: ElementRef;
    @ViewChild('buttonCloseModal') private buttonCloseModal: ElementRef;
    @ViewChild('buttonCloseConfirmModal') private buttonCloseConfirmModal: ElementRef;
    photoPreview: any;

    constructor(
        public translate: TranslateService,
        private formBuilder: FormBuilder,
        private router: Router,
        private appService: AppService,
        private toastr: ToastrService,
        private fireStorage: FireStorage,
        private messageService: MessageService) {
    }

    ngOnInit() {
        this.appService.getCountries().subscribe(
            data => {
                this.countries = data;
            }
        );
        this.getRivalsByPage();
        this.rivalForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(3)]],
            lastname: ['', [Validators.required, Validators.minLength(3)]],
            weight: ['', [Validators.required, Validators.minLength(2)]],
            sex: ['', [Validators.required]],
            country: [null, [Validators.required]],
        });
    }

    get data() {
        return this.rivalForm.controls;
    }

    openModal(action: string, rival?: any, i?: any) {
        delete this.fileData;
        this.action = action;
        this.submitted = false;
        if (action === 'add') {
            this.rivalForm.reset();
            this.photoPreview = '';
        } else {
            const rivalValues = {
                name: rival.name,
                lastname: rival.lastname,
                weight: rival.weight,
                country: {
                    country: rival.country,
                    code: rival.country_acronym,
                },
                sex: rival.sex,
            };
            this.photoPreview = rival.photoUrl;
            this.rivalForm.setValue(rivalValues);
            this.selectedRivalEdit = i;
        }
        this.openModalFormRival.nativeElement.click();
    }

    validateForm() {
        this.submitted = true;
        if (this.rivalForm.invalid) {
            this.toastr.error(this.messageService.checkFormFilledCorrectly);
            return;
        }
        const rival: Rival = {
            name: this.data.name.value,
            lastname: this.data.lastname.value,
            weight: this.data.weight.value,
            country: this.data.country.value.name,
            country_acronym: this.data.country.value.code,
            sex: this.data.sex.value
        };

        if (this.action === 'add') {
            if (this.fileData) {
                this.saveImage(rival);
            } else {
                this.saveRival(rival);
            }
        }
        if (this.action === 'edit') {
            rival.id = this.rivals[this.selectedRivalEdit].id;
            if (this.photoPreview !== rival.photoUrl && this.fileData) {
                this.saveImage(rival);
            } else {
                this.updateRival(rival);
            }
        }
    }

    private async saveImage(rival: Rival) {
        this.loading = true;
        const success = await this.fireStorage.saveRivalImage(this.fileData);
        if (success) {
            rival.photoUrl = success;
            this.action === 'add' ? this.saveRival(rival) : this.updateRival(rival);
        } else {
            this.loading = false;
            this.toastr.error(this.messageService.errorWasOccurred);
        }
    }

    private saveRival(rival: Rival) {
        this.loading = true;
        this.appService.saveRival(rival).then(
            success => {
                this.loading = false;
                this.submitted = false;
                this.rivalForm.reset();
                this.buttonCloseModal.nativeElement.click();
                this.getRivalsByPage();
                this.toastr.success(this.messageService.rivalSavedSuccessfully);
            }, () => {
                this.loading = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    private updateRival(rival: Rival) {
        this.loading = true;
        this.appService.updateRival(rival).then(
            success => {
                this.loading = false;
                this.submitted = false;
                this.rivalForm.reset();
                this.rivals[this.selectedRivalEdit] = success;
                this.buttonCloseModal.nativeElement.click();
                this.toastr.success(this.messageService.rivalSavedSuccessfully);
            }, () => {
                this.loading = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    deleteRival(rival: Rival) {
        this.loading = true;
        this.appService.deleteRival(rival).then(
            success => {
                this.rivals.splice(this.rivals.indexOf(rival), 1);
                this.buttonCloseConfirmModal.nativeElement.click();
                this.loading = false;
                this.toastr.info(this.messageService.rivalDeletedSuccessfully);
            }, error => {
                this.toastr.error(this.messageService.errorWasOccurred);
                this.loading = false;
            }
        );
    }

    chargeImage($event: any) {
        this.fileData = $event.target.files[0] as File;
        const mimeType = this.fileData.type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }
        const reader = new FileReader();
        reader.readAsDataURL(this.fileData);
        reader.onload = (event) => {
            this.photoPreview = reader.result;
        };
    }

    getRivalsByPage() {
        this.dataForPagination.pages = [];
        this.rivals = [];
        this.loadingRivals = true;
        this.appService.getRivalsByPage(this.dataForPagination.pageInUse).then(
            (success: PaginatedData) => {
                this.loadingRivals = false;
                this.rivals = success.dataPage;
                this.dataForPagination.allPages = success.allPages;
                if (this.dataForPagination.middlePage === 0) {
                    this.dataForPagination.middlePage = Math.trunc(this.dataForPagination.allPages / 2);
                }
                let pageController = {page: 0, selected: false};
                for (let i = 0; i < this.dataForPagination.allPages; i++) {
                    pageController.page = i;
                    if (this.dataForPagination.pageInUse === (i + 1)) {
                        pageController.selected = true;
                    }
                    this.dataForPagination.pages.push(pageController);
                    pageController = {page: 0, selected: false};
                }
            }, () => {
                this.loadingRivals = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    getPage(dataOfPagination: DataForPagination) {
        this.dataForPagination = dataOfPagination;
        this.getRivalsByPage();
    }

    getRivalCountry(form) {
        if (form.country.value) {
            return form.country.value.country;
        } else {
            return '';
        }
    }
}
