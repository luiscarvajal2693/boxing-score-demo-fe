import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { faTrash, faVenusMars, faFlag, faPlus, faEdit, faCamera } from '@fortawesome/free-solid-svg-icons';
import { Router } from '@angular/router';
import { AppService } from '../../../services/app.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Rival } from '../../../models/rival';
import { ToastrService } from 'ngx-toastr';
import { FireStorage } from '../../../services/fire.storage';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    faTrash = faTrash;
    faVenusMars = faVenusMars;
    faFlag = faFlag;
    faPlus = faPlus;
    faEdit = faEdit;
    faCamera = faCamera;

    countries: any[] = [];
    rivals: Rival[] = [];
    rivalForm: FormGroup;

    action = '';
    submitted = false;
    loading = false;
    loadingRivals = false;
    selectedRival: Rival;
    selectedRivalEdit: number;
    fileData: File;

    @ViewChild('openModalFormRival') private openModalFormRival: ElementRef;
    @ViewChild('buttonCloseModal') private buttonCloseModal: ElementRef;
    @ViewChild('buttonCloseConfirmModal') private buttonCloseConfirmModal: ElementRef;
    photoPreview: any;

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private appService: AppService,
        private toastr: ToastrService,
        private fireStorage: FireStorage) {
    }

    ngOnInit() {
        this.appService.getCountries().subscribe(
            data => {
                this.countries = data;
            }
        );
        this.getRivals();
        this.rivalForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(3)]],
            lastname: ['', [Validators.required, Validators.minLength(3)]],
            weight: ['', [Validators.required, Validators.minLength(2)]],
            sex: ['', [Validators.required]],
            country: [null, [Validators.required]],
        });
    }

    get data() {
        return this.rivalForm.controls;
    }

    getRivals() {
        this.loadingRivals = true;
        this.appService.getRivals().then(
            success => {
                this.loadingRivals = false;
                this.rivals = success;
            }, () => {
                this.loadingRivals = false;
                this.toastr.error('Sorry an error was occurred');
            }
        );
    }


    openModal(action: string, rival?: any, i?: any) {
        this.action = action;
        if (action === 'add') {
            this.rivalForm.reset();
        } else {
            const rivalValues = {
                name: rival.name,
                lastname: rival.lastname,
                weight: rival.weight,
                country: {
                    country: rival.country,
                    code: rival.country_acronym,
                },
                sex: rival.sex,
            };
            this.photoPreview = rival.photoUrl;
            this.rivalForm.setValue(rivalValues);
            this.selectedRivalEdit = i;
        }
        this.openModalFormRival.nativeElement.click();
    }

    validateForm() {
        this.submitted = true;
        if (this.rivalForm.invalid) {
            this.toastr.error('Check the form it seems that it has not been filled correctly');
            return;
        }
        const rival: Rival = {
            name: this.data.name.value,
            lastname: this.data.lastname.value,
            weight: this.data.weight.value,
            country: this.data.country.value.name,
            country_acronym: this.data.country.value.code,
            sex: this.data.sex.value
        };

        if (this.action === 'add') {
            this.saveImage(rival);
        }
        if (this.action === 'edit') {
            rival.id = this.rivals[this.selectedRivalEdit].id;
            if (this.photoPreview !== rival.photoUrl) {
                this.saveImage(rival);
            } else {
                this.updateRival(rival);
            }
        }
    }

    private async saveImage(rival: Rival) {
        this.loading = true;
        const success = await this.fireStorage.saveRivalImage(this.fileData);
        if (success) {
            console.log('-> success', success);
            rival.photoUrl = success;
            this.action === 'add' ? this.saveRival(rival) : this.updateRival(rival);
        } else {
            this.loading = false;
            this.toastr.error('Sorry an error was occurred');
        }
    }

    private saveRival(rival: Rival) {
        this.loading = true;
        this.appService.saveRival(rival).then(
            success => {
                this.loading = false;
                this.submitted = false;
                this.rivalForm.reset();
                this.buttonCloseModal.nativeElement.click();
                this.getRivals();
                this.toastr.success('The rival has been successfully saved');
            }, () => {
                this.loading = false;
                this.toastr.error('Sorry an error was occurred');
            }
        );
    }

    private updateRival(rival: Rival) {
        this.loading = true;
        this.appService.updateRival(rival).then(
            success => {
                this.loading = false;
                this.submitted = false;
                this.rivalForm.reset();
                this.rivals[this.selectedRivalEdit] = success;
                this.buttonCloseModal.nativeElement.click();
                this.toastr.success('The rival has been successfully saved');
            }, () => {
                this.loading = false;
                this.toastr.error('Sorry an error was occurred');
            }
        );
    }

    deleteRival(rival: Rival) {
        this.appService.deleteRival(rival).then(
            success => {
                this.rivals.splice(this.rivals.indexOf(rival), 1);
                this.buttonCloseConfirmModal.nativeElement.click();
                this.toastr.info('The rival has been successfully deleted');
            }, error => {
                this.toastr.error('Sorry an error was occurred');
            }
        );
    }

    chargeImage($event: any) {
        this.fileData = $event.target.files[0] as File;
        const mimeType = this.fileData.type;
        if (mimeType.match(/image\/*/) == null) {
            return;
        }
        const reader = new FileReader();
        reader.readAsDataURL(this.fileData);
        reader.onload = (event) => {
            this.photoPreview = reader.result;
        };
    }
}
