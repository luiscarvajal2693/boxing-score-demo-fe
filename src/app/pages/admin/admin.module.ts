import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminRoutingModule } from './admin-routing.module';
import { SharedModule } from '../shared.module';
import { ArchwizardModule } from 'angular-archwizard';
// Components
import { AdminComponent } from './admin.component';
import { RivalsComponent } from './rivals/rivals.component';
import { TournamentsComponent } from './tournaments/tournaments.component';
import { RoundsComponent } from './rounds/rounds.component';
import { BattleStepComponent } from '../../components/battle-step/battle-step.component';
import { HomeComponent } from './home/home.component';
import { ComponentsModule } from 'src/app/components/components.module';
import { NgxLoadingModule } from 'ngx-loading';
import {TranslateModule} from '@ngx-translate/core';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        AdminRoutingModule,
        FontAwesomeModule,
        NgSelectModule,
        NgbModule,
        ReactiveFormsModule,
        SharedModule,
        ArchwizardModule,
        ComponentsModule,
        ArchwizardModule,
        NgxLoadingModule,
        TranslateModule
    ],
    declarations: [
        AdminComponent,
        HomeComponent,
        RivalsComponent,
        RoundsComponent,
        BattleStepComponent,
        TournamentsComponent
    ]
})
export class AdminModule {
}
