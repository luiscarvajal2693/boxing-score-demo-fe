import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { faTrash, faPlus, faCalendar, faEdit } from '@fortawesome/free-solid-svg-icons';
import { Tournament } from '../../../models/tournament';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from '../../../services/app.service';
import { PaginatedData, DataForPagination } from 'src/app/models/paginatedData';
import {MessageService} from '../../../services/message.service';

@Component({
    selector: 'app-tournaments',
    templateUrl: './tournaments.component.html',
    styleUrls: ['./tournaments.component.scss']
})
export class TournamentsComponent implements OnInit {

    @ViewChild('openModalForm') private openModalForm: ElementRef;
    @ViewChild('buttonCloseModal') private buttonCloseModal: ElementRef;
    @ViewChild('buttonCloseConfirmModal') private buttonCloseConfirmModal: ElementRef;

    faTrash = faTrash;
    faEdit = faEdit;
    faPlus = faPlus;

    tournaments: Tournament[] = [];
    selectedTournamentDelete: Tournament;
    selectedTournamentEdit: number;
    tournamentForm: FormGroup;
    dataForPagination: DataForPagination = {
        pageInUse: 1,
        allPages: 0,
        pages: [],
        middlePage: 0,
        mainPage: 1,
    };

    action = '';
    loadingTournaments = false;
    loading = false;

    constructor(
        private formBuilder: FormBuilder,
        private toastr: ToastrService,
        private appService: AppService,
        private messageService: MessageService
    ) {
    }

    ngOnInit() {
        this.getTournamentsByPage();
        this.tournamentForm = this.formBuilder.group({
            name: ['', [Validators.required, Validators.minLength(5)]],
            start_date: ['', [Validators.required]],
            end_date: ['', [Validators.required]],
        });
    }

    get data() {
        return this.tournamentForm.controls;
    }

    getTournaments() {
        this.loadingTournaments = true;
        this.appService.getTournaments().then(
            success => {
                this.loadingTournaments = false;
                this.tournaments = success;
            }, () => {
                this.loadingTournaments = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    openModal(action: string, tourney?: any, i?: number) {
        this.action = action;
        if (action === 'add') {
            this.tournamentForm.reset();
        } else {
            console.log(tourney);
            const tourneyValues = {
                name: tourney.name,
                start_date: tourney.start_date,
                end_date: tourney.end_date
            };
            this.tournamentForm.setValue(tourneyValues);
            this.selectedTournamentEdit = i;
        }
        this.openModalForm.nativeElement.click();
    }

    validateTournament() {
        const form = this.tournamentForm;
        if (form.invalid) {
            this.toastr.error(this.messageService.checkFormFilledCorrectly);
            return;
        }
        if (form.value.start_date > form.value.end_date) {
            this.toastr.error(this.messageService.startDateMustBeLessEndDate, this.messageService.thereIsAProblem);
            return;
        }
        const tourney: Tournament = {
            name: this.data.name.value,
            start_date: this.data.start_date.value,
            end_date: this.data.end_date.value
        };
        if (this.action === 'add') {
            this.saveTournament(tourney);
        }
        if (this.action === 'edit') {
            tourney.id = this.tournaments[this.selectedTournamentEdit].id;
            this.updateTournament(tourney);
        }
    }

    private saveTournament(tourney: Tournament) {
        this.loading = true;
        this.appService.saveTournament(tourney).then(
            success => {
                this.loading = false;
                this.tournamentForm.reset();
                this.getTournamentsByPage();
                this.buttonCloseModal.nativeElement.click();
                this.toastr.success(this.messageService.tournamentSavedSuccessfully);
            }, () => {
                this.loading = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    private updateTournament(tourney: Tournament) {
        this.loading = true;

        this.appService.updateTournament(tourney).then(
            success => {
                console.log(success);
                this.loading = false;
                this.tournaments[this.selectedTournamentEdit] = success;
                this.tournamentForm.reset();
                this.buttonCloseModal.nativeElement.click();
                this.toastr.success(this.messageService.tournamentSavedSuccessfully);
            }, () => {
                this.loading = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    deleteTournament() {
        this.appService.deleteTournament(this.selectedTournamentDelete).then(
            success => {
                this.tournaments.splice(this.tournaments.indexOf(this.selectedTournamentDelete), 1);
                this.buttonCloseConfirmModal.nativeElement.click();
                this.toastr.info(this.messageService.tournamentDeletedSuccessfully);
            }, error => {
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    getTournamentsByPage() {
        this.dataForPagination.pages = [];
        this.tournaments = [];
        this.loadingTournaments = true;
        this.appService.getTournamentsByPage(this.dataForPagination.pageInUse).then(
            (success: PaginatedData) => {
                this.loadingTournaments = false;
                this.tournaments = success.dataPage;
                this.dataForPagination.allPages = success.allPages;
                if ( this.dataForPagination.middlePage === 0) {
                    this.dataForPagination.middlePage = Math.trunc(this.dataForPagination.allPages / 2);
                }
                let pageController = { page: 0, selected: false };
                for (let i = 0; i < this.dataForPagination.allPages; i++) {
                    pageController.page = i;
                    if (this.dataForPagination.pageInUse === (i + 1)) {
                        pageController.selected = true;
                    }
                    this.dataForPagination.pages.push(pageController);
                    pageController = { page: 0, selected: false };
                }
            }, () => {
                this.loadingTournaments = false;
                this.toastr.error(this.messageService.errorWasOccurred);
            }
        );
    }

    getPage(dataOfPagination: DataForPagination) {
        this.dataForPagination = dataOfPagination;
        this.getTournamentsByPage();
    }

}
