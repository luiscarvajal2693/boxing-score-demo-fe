import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BattleTableComponent } from '../components/battle-table/battle-table.component';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { BattleHomeStepComponent } from '../components/battle-home-step/battle-home-step.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        FontAwesomeModule,
    ],
    declarations: [
        BattleTableComponent,
        BattleHomeStepComponent
    ],
    providers: [],
    exports: [
        BattleTableComponent,
        BattleHomeStepComponent
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule {
}
