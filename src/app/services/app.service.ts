import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Rival } from '../models/rival';
import { Match } from '../models/match';
import { Tournament } from '../models/tournament';

const IS_LOGGED = 'is_logged';

@Injectable()
export class AppService {

    /* this json us only for test */
    apiUrl: string;
    jsonUrl: string;
    countriesJsonUrl: string;
    rivalsJsonUrl: string;
    languages: string[];

    constructor(private http: HttpClient) {
    }

    public isLogged(): boolean {
        return localStorage.getItem(IS_LOGGED) === 'Y';
    }


    public setLogged(isLogged: boolean) {
        localStorage.setItem(IS_LOGGED, isLogged ? 'Y' : 'N');
    }

    public login(username: string, password: string): Promise<any> {
        return this.http.post(`${this.apiUrl}/auth/api/sign-in`, {
            username,
            password
        }).toPromise();
    }

    public getJSON(): Observable<any> {
        return this.http.get(this.jsonUrl);
    }

    public getCountries(): Observable<any> {
        return this.http.get(this.countriesJsonUrl);
    }

    public getLanguages(): string[] {
        return this.languages;
    }

    public getCategories(): Promise<any> {
        return this.http.get(`${this.apiUrl}/category/api/get-all`).toPromise();
    }

    public getBattlesRoundByCategory(id: string, tournamentId: string): Promise<any> {
        return this.http.get(`${this.apiUrl}/round/api/battles-by-category/${id}/tournament/${tournamentId}`).toPromise();
    }

    public updateMatch(match: Match): Promise<any> {
        return this.http.put(`${this.apiUrl}/match/api/update`, match).toPromise();
    }


    // Tournaments

    public getTournaments(): Promise<any> {
        return this.http.get(`${this.apiUrl}/tournament/api/get-all`).toPromise();
    }

    public getTournamentsByPage(page): Promise<any> {
        return this.http.get(`${this.apiUrl}/tournament/api/get-by-page/:${page}`).toPromise();
    }

    public saveTournament(tournament: Tournament): Promise<any> {
        return this.http.post(`${this.apiUrl}/tournament/api/save`, tournament).toPromise();
    }

    public updateTournament(tournament: Tournament): Promise<any> {
        return this.http.put(`${this.apiUrl}/tournament/api/update`, tournament).toPromise();
    }

    public deleteTournament(tournament: Tournament): Promise<any> {
        return this.http.delete(`${this.apiUrl}/tournament/api/delete/${tournament.id}`).toPromise();
    }


    // Rivals

    public getRivals(): Promise<any> {
        return this.http.get(`${this.apiUrl}/rival/api/get-all`).toPromise();
    }

    public getRivalsByPage(page): Promise<any> {
        return this.http.get(`${this.apiUrl}/rival/api/get-by-page/:${page}`).toPromise();
    }

    public getRivalsByCategory(id: string): Promise<any> {
        return this.http.get(`${this.apiUrl}/rival/api/get-by-category/${id}`).toPromise();
    }

    public saveRival(rival: Rival): Promise<any> {
        return this.http.post(`${this.apiUrl}/rival/api/save`, rival).toPromise();
    }

    public updateRival(rival: Rival): Promise<any> {
        return this.http.put(`${this.apiUrl}/rival/api/update`, rival).toPromise();
    }

    public deleteRival(rival: Rival): Promise<any> {
        return this.http.delete(`${this.apiUrl}/rival/api/delete/${rival.id}`).toPromise();
    }


    // Rounds
    public getAllRounds(): Promise<any> { /* Get List of all Rounds */
        return this.http.get(`${this.apiUrl}/round/api/get-all`).toPromise();
    }

    public getRoundByCategory(id: string, tournamentId: string): Promise<any> {
        return this.http.get(`${this.apiUrl}/match/api/get-by-category/${id}/tournament/${tournamentId}`).toPromise();
    }

    public saveRound(matches: Match[]): Promise<any> {
        return this.http.post(`${this.apiUrl}/round/api/create`, matches).toPromise();
    }

    public deleteRoundByCategory(id: string, tournamentId: string): Promise<any> {
        return this.http.delete(`${this.apiUrl}/round/api/delete-by-category/${id}/tournament/${tournamentId}`).toPromise();
    }

}
