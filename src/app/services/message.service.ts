import { Injectable } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class MessageService {

    // tslint:disable-next-line:variable-name
    private _error;
    // tslint:disable-next-line:variable-name
    private _done;
    // tslint:disable-next-line:variable-name
    private _save;
    // tslint:disable-next-line:variable-name
    private _checkFormFilledCorrectly;
    // tslint:disable-next-line:variable-name
    private _errorWasOccurred;
    // tslint:disable-next-line:variable-name
    private _rivalSavedSuccessfully;
    // tslint:disable-next-line:variable-name
    private _rivalDeletedSuccessfully;
    // tslint:disable-next-line:variable-name
    private _tournamentSavedSuccessfully;
    // tslint:disable-next-line:variable-name
    private _tournamentDeletedSuccessfully;
    // tslint:disable-next-line:variable-name
    private _startDateMustBeLessEndDate;
    // tslint:disable-next-line:variable-name
    private _thereIsAProblem;
    // tslint:disable-next-line:variable-name
    private _matchesDeleteSuccessfully;
    // tslint:disable-next-line:variable-name
    private _youMustAssignDateOfBattles;
    // tslint:disable-next-line:variable-name
    private _roundCreatedSuccessfully;
    // tslint:disable-next-line:variable-name
    private _dateSavedSuccessfully;
    // tslint:disable-next-line:variable-name
    private _alreadyLiveBattle;

    constructor(private translate: TranslateService) {
    }

    public async configure() {
        this._error = await this.translate.get('message.error').toPromise();
        this._done = await this.translate.get('message.done').toPromise();
        this._save = await this.translate.get('message.save').toPromise();
        this._checkFormFilledCorrectly = await this.translate.get('message.checkFormFilledCorrectly').toPromise();
        this._errorWasOccurred = await this.translate.get('message.errorWasOccurred').toPromise();
        this._rivalSavedSuccessfully = await this.translate.get('message.rivalSavedSuccessfully').toPromise();
        this._rivalDeletedSuccessfully = await this.translate.get('message.rivalDeletedSuccessfully').toPromise();
        this._tournamentSavedSuccessfully = await this.translate.get('message.tournamentSavedSuccessfully').toPromise();
        this._tournamentDeletedSuccessfully = await this.translate.get('message.tournamentDeletedSuccessfully').toPromise();
        this._startDateMustBeLessEndDate = await this.translate.get('message.startDateMustBeLessEndDate').toPromise();
        this._thereIsAProblem = await this.translate.get('message.thereIsAProblem').toPromise();
        this._matchesDeleteSuccessfully = await this.translate.get('message.matchesDeleteSuccessfully').toPromise();
        this._youMustAssignDateOfBattles = await this.translate.get('message.youMustAssignDateOfBattles').toPromise();
        this._roundCreatedSuccessfully = await this.translate.get('message.roundCreatedSuccessfully').toPromise();
        this._dateSavedSuccessfully = await this.translate.get('message.dateSavedSuccessfully').toPromise();
        this._alreadyLiveBattle = await this.translate.get('message.alreadyLiveBattle').toPromise();
    }

    get checkFormFilledCorrectly() {
        return this._checkFormFilledCorrectly;
    }

    get errorWasOccurred() {
        return this._errorWasOccurred;
    }

    get rivalSavedSuccessfully() {
        return this._rivalSavedSuccessfully;
    }

    get rivalDeletedSuccessfully() {
        return this._rivalDeletedSuccessfully;
    }

    get tournamentSavedSuccessfully() {
        return this._tournamentSavedSuccessfully;
    }

    get tournamentDeletedSuccessfully() {
        return this._tournamentDeletedSuccessfully;
    }

    get startDateMustBeLessEndDate() {
        return this._startDateMustBeLessEndDate;
    }

    get thereIsAProblem() {
        return this._thereIsAProblem;
    }

    get error() {
        return this._error;
    }

    get done() {
        return this._done;
    }

    get save() {
        return this._save;
    }

    get matchesDeleteSuccessfully() {
        return this._matchesDeleteSuccessfully;
    }

    get youMustAssignDateOfBattles() {
        return this._youMustAssignDateOfBattles;
    }

    get roundCreatedSuccessfully() {
        return this._roundCreatedSuccessfully;
    }

    get dateSavedSuccessfully() {
        return this._dateSavedSuccessfully;
    }

    get alreadyLiveBattle() {
        return this._alreadyLiveBattle;
    }
}
