import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AppService} from './app.service';
import {MessageService} from './message.service';

const EN = 'en';
const DEFAULT_LANGUAGE = 'default_language';

@Injectable({
    providedIn: 'root'
})
export class LanguageService {

    constructor(private translate: TranslateService, private appService: AppService, private messageService: MessageService) {
    }

    public configure() {
        this.translate.setDefaultLang(this.getCurrentLanguage());
    }

    public getSystemLanguage(): string {
        // @ts-ignore
        let userLang = navigator.language || navigator.userLanguage;
        if (userLang) {
            userLang = userLang.substr(0, 2);
            if (this.appService.getLanguages().indexOf(userLang.toUpperCase()) === -1) {
                userLang = EN;
            }
        } else {
            userLang = EN;
        }
        return userLang;
    }

    // Return language on lower case
    public getCurrentLanguage(): string {
        return localStorage.getItem(DEFAULT_LANGUAGE) || this.getSystemLanguage();
    }

    // Should save a lower case
    public setDefaultLanguage(language: string) {
        this.translate.use(language);
        this.messageService.configure();
        localStorage.setItem(DEFAULT_LANGUAGE, language);
    }
}
