import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {NgSelectModule} from '@ng-select/ng-select';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AngularFireModule} from '@angular/fire';

import {map} from 'rxjs/operators';
import {Md5} from 'ts-md5/dist/md5';
import {ToastrModule} from 'ngx-toastr';

import {AppRoutingModule} from './app-routing.module';
import {AppService} from './services/app.service';
import {SharedModule} from './pages/shared.module';
import {firebaseConfig} from '../environments/environment';
// Bootstrap
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {BootstrapFormComponent} from './bootstrap-form/bootstrap-form.component';
import {BootstrapTableComponent} from './bootstrap-table/bootstrap-table.component';
// Components
import {AppComponent} from './app.component';
import {LoginComponent} from './pages/login/login.component';
import {HomeComponent} from './pages/home/home.component';
import {JumbotronComponent} from './jumbotron/jumbotron.component';
import {AngularFireStorageModule} from '@angular/fire/storage';
// Translation
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgxLoadingModule } from 'ngx-loading';


function load(http: HttpClient, config: AppService): (() => Promise<boolean>) {
    return (): Promise<boolean> => {
        return new Promise<boolean>((resolve: (a: boolean) => void): void => {
            http.get('./assets/config.json')
                .pipe(
                    map((x: AppService) => {
                        config.apiUrl = x.apiUrl;
                        config.jsonUrl = x.jsonUrl;
                        config.countriesJsonUrl = x.countriesJsonUrl;
                        config.rivalsJsonUrl = x.rivalsJsonUrl;
                        config.languages = x.languages;
                        resolve(true);
                    })
                ).subscribe();
        });
    };
}

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        AppRoutingModule,
        FormsModule,
        NgbModule,
        ReactiveFormsModule,
        CommonModule,
        FontAwesomeModule,
        HttpClientModule,
        NgSelectModule,
        SharedModule,
        ToastrModule.forRoot({
            positionClass: 'toast-bottom-center'
        }),
        AngularFireStorageModule,
        AngularFireModule.initializeApp(firebaseConfig),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: (http: HttpClient) => {
                    return new TranslateHttpLoader(http);
                },
                deps: [HttpClient]
            }
        }),
        NgxLoadingModule
    ],
    declarations: [
        AppComponent,
        JumbotronComponent,
        BootstrapFormComponent,
        BootstrapTableComponent,
        LoginComponent,
        HomeComponent,
    ],
    exports: [],
    providers: [
        Md5,
        AppService, {
            provide: APP_INITIALIZER,
            useFactory: load,
            deps: [
                HttpClient,
                AppService
            ],
            multi: true
        }],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule {
}
