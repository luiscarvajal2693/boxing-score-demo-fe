import {Rival} from './rival';

export interface Match {
    id?: string;
    rival1?: Rival;
    rival2?: Rival;
    rival1_score?: string;
    rival2_score?: string;
    rival1_id?: string;
    rival2_id?: string;
    tournament_id?: string;
    category_id?: string;
    category?: string;
    next_match_id?: string;
    step?: number;
    winner?: number;
    date?: string;
    live?: boolean;
}

