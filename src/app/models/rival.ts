export interface Rival {
  id?: any;
  name?: string;
  lastname?: string;
  weight?: string;
  sex?: string;
  country?: string;
  country_acronym?: string;
  photoUrl?: string;
  disable?: boolean;
}

