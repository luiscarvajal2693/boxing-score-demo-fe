export interface Category {
    sex?: string;
    name?: string;
    heavier?: boolean;
    weight?: number;
    id?: string;
}

