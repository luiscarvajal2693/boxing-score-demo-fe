import {Step} from './step';

export interface Round {
    step1: Step;
    step2: Step;
    step3: Step;
    step4: Step;
}
