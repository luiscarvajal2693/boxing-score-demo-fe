export interface PaginatedData {
  page: number;
  dataPage: any[];
  allPages: number;
  firstIndex: number;
  lastIndex: number;
  ItemsPerPage: number;
}
export interface DataForPagination  {
    pageInUse: number;
    allPages: number;
    pages: any[];
    middlePage: number;
    mainPage: number;
}

