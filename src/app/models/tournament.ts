export interface Tournament {
    id?: any;
    name?: string;
    start_date?: string;
    end_date?: string;
}

