import {Match} from './match';

export interface Step {
    date?: any;
    dateString?: any;
    matches?: Match[];
}

