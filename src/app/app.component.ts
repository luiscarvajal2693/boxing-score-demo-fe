import {Component, OnInit} from '@angular/core';
import {LanguageService} from './services/language.service';
import {MessageService} from './services/message.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(private languageService: LanguageService, private messageService: MessageService) {
        languageService.configure();
        messageService.configure();
    }
}
